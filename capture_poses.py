import argparse
import os
import sys
import pathlib
import datetime
import cv2
import torch
import numpy as np

sys.path.append(os.path.join(os.path.dirname(__file__), 'lw_pose_est'))

from lw_pose_est.models.with_mobilenet import PoseEstimationWithMobileNet
from lw_pose_est.modules.load_state import load_state
from lw_pose_est.modules.pose import Pose, track_poses

from common import VideoReader, infer_poses, save_pose, pose_names

def get_next_folder():
    out_path = os.path.join(os.path.dirname(__file__), 'train_poses')
    pathlib.Path(out_path).mkdir(parents=False, exist_ok=True)

    train_dirs = os.listdir(out_path)
    max_number = 0
    for it in train_dirs:
        if not os.path.isdir(os.path.join(out_path, it)):
            continue
        if not it.isnumeric():
            continue
        number = int(it)
        max_number = max(max_number, number)
    
    out_path = os.path.join(out_path, str(max_number+1))
    pathlib.Path(out_path).mkdir(parents=False, exist_ok=False)
    return out_path

def run_demo(net, image_provider, height_size, gpu, track, smooth):
    net = net.eval()
    if gpu:
        net = net.cuda()

    out_path = get_next_folder()

    previous_poses = []
    delay = 1
    pose_idx = 0
    for img in image_provider:
        #orig_img = img.copy()
        
        current_poses = infer_poses(net, img, height_size, gpu)

        if track:
            track_poses(previous_poses, current_poses, smooth=smooth)
            previous_poses = current_poses
        for pose in current_poses:
            pose.draw(img)
        #img = cv2.addWeighted(orig_img, 0.6, img, 0.4, 0)

        ## show bounding box and tracking
        #for pose in current_poses:
        #    cv2.rectangle(img, (pose.bbox[0], pose.bbox[1]),
        #                  (pose.bbox[0] + pose.bbox[2], pose.bbox[1] + pose.bbox[3]), (0, 255, 0))
        #    if track:
        #        cv2.putText(img, 'id: {}'.format(pose.id), (pose.bbox[0], pose.bbox[1] - 16),
        #                    cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255))
        
        # visu image: flip horizontally (but save original instead!)
        img_flipped = cv2.flip(img, 1) # mirror horizontally
        cv2.putText(img_flipped, f'{pose_names[pose_idx]}', (20, 20), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255))

        cv2.imshow('Record poses', img_flipped)
        key = cv2.waitKey(delay)
        if key == 27:  # esc
            break
        elif key == ord(' '): # space
            # save pose
            if len(current_poses) == 1:
                pose = current_poses[0]
    
                label = pose_names[pose_idx]
                timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
                filename_base = f"pose_{label}_{timestamp}"
                save_pose(pose, label, img, os.path.join(out_path, filename_base + "_kp"))

                save_skel_img = True
                if save_skel_img:
                    skel_img = np.ones_like(img) * 255
                    pose.draw(skel_img)
                    cv2.imwrite(os.path.join(out_path, filename_base + '_skel.png'), skel_img)
                
                pose_idx += 1
                if pose_idx >= len(pose_names):
                    pose_idx = 0
            else:
                print("Couldn't save, unexpected number of poses: ", len(current_poses))
    # close the window and free memory
    cv2.destroyAllWindows()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='''Lightweight human pose estimation python demo.
                       This is just for quick results preview.
                       Please, consider c++ demo for the best performance.''')
    parser.add_argument('--checkpoint-path', type=str, default='models/checkpoint_iter_370000.pth', help='path to the checkpoint')
    parser.add_argument('--height-size', type=int, default=256, help='network input layer height size')
    parser.add_argument('--video', type=str, default=0, help='path to video file or camera id')
    parser.add_argument('--images', nargs='+', default='', help='path to input image(s)')
    parser.add_argument('--gpu', default=True, action='store_true', help='run network inference on gpu')
    parser.add_argument('--track', type=int, default=1, help='track pose id in video')
    parser.add_argument('--smooth', type=int, default=1, help='smooth pose keypoints')
    args = parser.parse_args()

    if args.video == '':
        raise ValueError('Video input has to be provided')

    net = PoseEstimationWithMobileNet()
    if args.gpu:
        target='cuda'
    else:
        target='cpu'
    checkpoint = torch.load(args.checkpoint_path, map_location=target)
    load_state(net, checkpoint)

    frame_provider = VideoReader(args.video)

    Pose.color = [0, 100, 200]
    run_demo(net, frame_provider, args.height_size, args.gpu, args.track, args.smooth)
